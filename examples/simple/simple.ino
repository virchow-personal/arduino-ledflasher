

#include "ledflasher.h"
#define LED_PIN 10

LEDFlasher flasher;


void setup() {
  flasher.begin(LED_PIN);
  flasher.flash(5);
}

void loop() {
  flasher.update();
  // do other stuff
}
