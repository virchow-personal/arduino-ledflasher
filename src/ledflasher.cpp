

#include "ledflasher.h"


void LEDFlasher::begin(uint8_t pin) {
  _pin = pin;
  pinMode(_pin, OUTPUT);
  _off();
}

void LEDFlasher::flash() {
  this->flash(-1);
}

void LEDFlasher::flash(int32_t count) {
  _counter = count;
  _flashing = true;
  _on();
}

void LEDFlasher::stop() {
  _flashing = false;
  _off();
}

void LEDFlasher::update() {
  if (_flashing) {
    if (_timer > _flashtime) {
      _set(!_state);
      if (!_state && _counter > -1 && --_counter == 0) {
        stop();
      }
    }
  } else if (_state) {
    _off();
  }
}


void LEDFlasher::_set(bool state) {
  digitalWrite(_pin, state ? _onstate : _offstate); 
  _state = state;
  _timer = 0;
}

void LEDFlasher::_on() {
  _set(true);
}

void LEDFlasher::_off() {
  _set(false);
}

